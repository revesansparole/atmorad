========================
atmorad
========================

.. {# pkglts, doc

.. image:: https://revesansparole.gitlab.io/atmorad/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/atmorad/

.. image:: https://revesansparole.gitlab.io/atmorad/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/atmorad/0.0.1/

.. image:: https://revesansparole.gitlab.io/atmorad/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/atmorad

.. image:: https://badge.fury.io/py/atmorad.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/atmorad

.. #}
.. {# pkglts, itkpkg, after doc
develop: |dvlp_build|_ |dvlp_coverage|_

.. |dvlp_build| image:: https://gitlab.com/revesansparole/atmorad/badges/develop/pipeline.svg
.. _dvlp_build: https://gitlab.com/revesansparole/atmorad/commits/develop

.. |dvlp_coverage| image:: https://gitlab.com/revesansparole/atmorad/badges/develop/coverage.svg
.. _dvlp_coverage: https://gitlab.com/revesansparole/atmorad/commits/develop


master: |master_build|_ |master_coverage|_

.. |master_build| image:: https://gitlab.com/revesansparole/atmorad/badges/master/pipeline.svg
.. _master_build: https://gitlab.com/revesansparole/atmorad/commits/master

.. |master_coverage| image:: https://gitlab.com/revesansparole/atmorad/badges/master/coverage.svg
.. _master_coverage: https://gitlab.com/revesansparole/atmorad/commits/master
.. #}

Set of formalisms to deal with radiation in the atmosphere for agronomy and ecology mainly

