from datetime import datetime, timedelta
from math import radians

import pytest
from atmorad.astrosun import solar_constant
from atmorad.maleki2017 import daily_extraterrestrial_radiation, hourly_extraterrestrial_radiation


def test_daily_extraterrestrial_radiation_is_between_0_and_solar_constant():
    for day in (datetime(2019, 1, 1), datetime(2020, 6, 1), datetime(2021, 9, 1)):
        for latitude in (0, 10, 45):
            assert 0 <= daily_extraterrestrial_radiation(day, radians(latitude)) <= solar_constant


def test_hourly_extraterrestrial_radiation_sums_to_daily_extraterrestrial_radiation():
    for day in (datetime(2019, 1, 1), datetime(2020, 6, 1), datetime(2021, 9, 1)):
        for latitude in (0, 10, 45):
            lat = radians(latitude)
            rg_day = daily_extraterrestrial_radiation(day, lat)

            rg_sum = sum(hourly_extraterrestrial_radiation(day + timedelta(hours=h), lat) for h in range(24))

            assert rg_sum == pytest.approx(rg_day, abs=0.5)
