import pytest
from atmorad.baig1991 import day_course


def test_day_course_returns_different_values_per_hour():
    assert day_course(30, -1, 12) != day_course(30, 0, 12)


def test_day_course_sums_to_daily():
    rg_day = 30  # [MJ.m-2]
    dt = 0.1
    tsns = [v * dt for v in range(-120, 120)]

    assert sum(day_course(rg_day, tsn, 15) * dt * 3600e-6 for tsn in tsns) == pytest.approx(rg_day, abs=0.2)
