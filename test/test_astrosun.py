from datetime import datetime, timedelta
from math import degrees, pi, radians

import pytest
from atmorad.astrosun import (day_length, declination, elevation, solar_hour, solar_hour_angle, solar_noon, sunrise,
                              sunset)
from ephem import next_summer_solstice, next_winter_solstice


def test_declination_increases_from_winter_solstice_till_summer_solstice():
    winter_solstice = datetime(*next_winter_solstice('2017').tuple()[:3])
    summer_solstice = datetime(*next_summer_solstice('2018').tuple()[:3])
    day = timedelta(days=1)

    date = winter_solstice + day
    while date < summer_solstice:
        assert declination(date + day) > declination(date)
        date = date + day


def test_declination_decreases_from_summer_solstice_till_winter_solstice():
    summer_solstice = datetime(*next_summer_solstice('2018').tuple()[:3])
    winter_solstice = datetime(*next_winter_solstice('2018').tuple()[:3])
    day = timedelta(days=1)

    date = summer_solstice + day
    while date < winter_solstice:
        assert declination(date + day) < declination(date)
        date = date + day


def test_elevation_is_zero_for_sunrise_and_sunset():
    date = datetime(2018, 4, 15)

    # northern hemisphere
    lat = radians(43)
    assert degrees(elevation(sunrise(date, lat), lat)) == pytest.approx(0, abs=1)
    assert degrees(elevation(sunset(date, lat), lat)) == pytest.approx(0, abs=1)

    # northern hemisphere
    lat = radians(-30)
    assert degrees(elevation(sunrise(date, lat), lat)) == pytest.approx(0, abs=1)
    assert degrees(elevation(sunset(date, lat), lat)) == pytest.approx(0, abs=1)


def test_elevation_is_90_for_midday_when_latitude_is_declination():
    # northern hemisphere
    date = datetime(2018, 6, 15)
    lat = declination(date)

    rt = sunrise(date, lat)
    st = sunset(date, lat)
    midday = rt + (st - rt) / 2

    assert degrees(elevation(midday, lat)) == pytest.approx(90, abs=1)

    # souhern hemisphere
    date = datetime(2018, 12, 15)
    lat = declination(date)

    rt = sunrise(date, lat)
    st = sunset(date, lat)
    midday = rt + (st - rt) / 2

    assert degrees(elevation(midday, lat)) == pytest.approx(90, abs=1)


def test_day_length_increases_from_winter_solstice_till_summer_solstice():
    lat = radians(43)
    winter_solstice = datetime(*next_winter_solstice('2017').tuple()[:3])
    summer_solstice = datetime(*next_summer_solstice('2018').tuple()[:3])
    day = timedelta(days=1)

    date = winter_solstice + day
    while date < summer_solstice:
        assert day_length(date + day, lat) > day_length(date, lat)
        date = date + day


def test_day_length_decreases_from_summer_solstice_till_winter_solstice():
    lat = radians(43)
    summer_solstice = datetime(*next_summer_solstice('2018').tuple()[:3])
    winter_solstice = datetime(*next_winter_solstice('2018').tuple()[:3])
    day = timedelta(days=1)

    date = summer_solstice + day
    while date < winter_solstice:
        assert day_length(date + day, lat) < day_length(date, lat)
        date = date + day


def test_elevation_occurs_later_westward():
    lat = radians(43)

    date = datetime(2018, 6, 1, 12)

    assert elevation(date, lat, 0.) == pytest.approx(elevation(date - timedelta(hours=1), lat, radians(360 / 24)),
                                                     abs=1e-3)


def test_sunrise_occurs_later_westward():
    lat = radians(43)

    day = datetime(2018, 6, 1)

    events = [sunrise(day, latitude=lat, longitude=radians(longitude)) for longitude in (30, 15, 0, -15, -30)]

    assert sorted(events) == events


def test_solar_noon_is_between_sunrise_and_sunset():
    lat = radians(43)
    day = datetime(2018, 6, 1)

    for longitude in (-45, 0, 45):
        long = radians(longitude)
        sunr = sunrise(day, lat, long)
        suns = sunset(day, lat, long)

        assert sunr < solar_noon(day, lat, long) < suns

        noon_estim = sunr + (suns - sunr) / 2
        assert abs((noon_estim - solar_noon(day, lat, long)).total_seconds()) < 60


def test_sunset_occurs_later_westward():
    lat = radians(43)

    day = datetime(2018, 6, 1)

    events = [sunset(day, latitude=lat, longitude=radians(longitude)) for longitude in (30, 15, 0, -15, -30)]

    assert sorted(events) == events


def test_solar_hour_is_negative_in_the_morning():
    lat = radians(43)
    assert solar_hour(sunrise(datetime(2018, 6, 1), lat), lat) < 0.


def test_solar_hour_is_positive_in_the_afternoon():
    lat = radians(43)
    assert solar_hour(sunset(datetime(2018, 6, 1), lat), lat) > 0.


def test_solar_hour_is_zero_around_solar_noon():
    lat = radians(43)
    assert solar_hour(datetime(2018, 6, 1, 12), lat) == pytest.approx(0., abs=0.3)


def test_solar_hour_angle_varies_between_minus_pi_and_pi():
    lat = radians(43)
    day = datetime(2018, 6, 1)

    for h in range(0, 24):
        assert -pi <= solar_hour_angle(day + timedelta(hours=h), lat) <= pi
