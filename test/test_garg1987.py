from math import pi

import pytest
from atmorad.garg1987 import day_course


def sha(h):
    return h * pi / 12


def test_hourly_returns_different_values_per_hour():
    assert day_course(30, sha(-1), sha(7)) != day_course(30, sha(0), sha(7))


def test_hourly_sums_to_daily():
    rg_day = 30  # [MJ.m-2]
    dt = 0.1
    tsns = [v * dt for v in range(-120, 120)]

    assert sum(day_course(rg_day, sha(tsn), sha(7)) * dt * 3600e-6 for tsn in tsns) == pytest.approx(rg_day, abs=0.4)
