from atmorad.boland2001 import diffuse_fraction


def test_diffuse_fraction_is_between_0_and_1():
    for kt in (0., 0.1, 0.5, 0.7, 0.8, 0.9, 1., 1.5):
        assert 0 <= diffuse_fraction(kt) <= 1
