"""
Implementation of formalisms from Garg 1987 (as expressed in Yao 2015).
"""
from math import cos, pi, sin


def day_course(rg, w, ws):
    """Instantaneous value of irradiance extrapolated from daily value

    References: Yao 2015, eq11

    Args:
        rg (float): [MJ.m-2] total daily irradiance
        w (float): [rad] sun_hour_angle
        ws (float): [rad] sunset_hour_angle

    Returns:
        (float): [W.m-2] Instantaneous irradiance
    """
    ratio = pi / 24 * (cos(w) - cos(ws)) / (sin(ws) - ws * cos(ws)) - 8e-3 * sin(3 * (w - 0.65))

    return max(0., ratio * rg / 3600e-6)
