"""
Implementation of formalisms from Maleki 2017.
"""
from datetime import datetime
from math import cos, pi, sin

from .astrosun import declination, solar_constant, solar_hour_angle, sunrise


def hourly_extraterrestrial_radiation(date, latitude, longitude=0.):
    """Amount of radiation reaching earth without atmosphere between date and date + 1h

    References: Maleki (2017) p4

    Args:
        date (datetime): date and time of computation (solar time if longitude=0 else UTC)
        latitude (float): [rad] Latitude north of the Equator
        longitude (float): [rad] Longitude positive East of Greenwich

    Returns:
        (float): [MJ.m-2.h-1]
    """
    w1 = solar_hour_angle(date, latitude, longitude)
    w2 = w1 + pi / 12
    decl = float(declination(date))
    n = date.timetuple().tm_yday
    # gamma = 2 * pi * (n - 1) / 365
    # e0 = (1.000110
    #       + 0.034221 * cos(gamma) + 0.001280 * sin(gamma)
    #       + 0.000719 * cos(2 * gamma) + 0.000077 * sin(2 * gamma))
    e0 = 1 + 0.003 * cos(2 * pi * n / 365)

    ratio = sin(latitude) * cos(decl) * (sin(w2) - sin(w1)) + (w2 - w1) * sin(latitude) * sin(decl)

    return max(0., 12 * 3600 * 1e-6 * solar_constant * ratio * e0 / pi)


def daily_extraterrestrial_radiation(day, latitude):
    """Amount of radiation reaching earth without atmosphere

    References: Maleki (2017) p20 with correction of typo cos vs sin

    Args:
        day (datetime): day of computation (the HH:MM:SS will be removed)
        latitude (float): [rad] Latitude north of the Equator

    Returns:
        (float): [MJ.m-2.day-1]
    """
    ws = solar_hour_angle(sunrise(day, latitude), latitude) * -1  # change of sign convention between authors
    decl = float(declination(day))
    n = day.timetuple().tm_yday
    e0 = 1 + 0.003 * cos(2 * pi * n / 365)

    ratio = sin(latitude) * cos(decl) * sin(ws) + ws * sin(latitude) * sin(decl)

    return 24 * 3600 * 1e-6 * solar_constant * ratio * e0 / pi
