"""
Implementation of formalisms from Baig 1991 (as expressed in Yao 2015).
"""
from math import cos, exp, pi, sqrt


def day_course(rg, tsn, day_length):
    """Instantaneous value of irradiance extrapolated from daily value

    References: Yao 2015, eq17 et eq18

    Args:
        rg (float): [MJ.m-2] total daily irradiance
        tsn (float): [h] time from solar noon
        day_length (float): [h] length of daylight

    Returns:
        (float): [W.m-2] Instantaneous irradiance
    """
    sig = 0.21 * day_length + 0.26
    ratio = (exp(-tsn ** 2 / (2 * sig ** 2)) + cos(pi * tsn / (day_length - 1))) / (2 * sig * sqrt(2 * pi))

    return max(0., ratio * rg / 3600e-6)
