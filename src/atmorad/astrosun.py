"""
Library of functions for astronomical computations linked to the sun
"""
from datetime import datetime
from math import pi

from ephem import Observer, Sun

solar_constant = 1361
"""[W.m-2] energy radiated by the sun and received on flat surface perpendicular
to solar rays on top of atmosphere.

References: https://en.wikipedia.org/wiki/Solar_constant
"""


def sunrise(day, latitude, longitude=0.):
    """Time of sunrise.

    Args:
        day (datetime): day of computation (the HH:MM:SS will be removed)
        latitude (float): [rad] Latitude north of the Equator
        longitude (float): [rad] Longitude positive East of Greenwich

    Returns:
        (datetime): Time of sunrise that day
    """
    sun = Sun()
    obs = Observer()
    obs.lat = latitude
    obs.lon = longitude
    obs.date = day.date()

    return obs.next_rising(sun).datetime()


def solar_noon(day, latitude, longitude=0.):
    """Time of maximum elevation.

    Args:
        day (datetime): day of computation (the HH:MM:SS will be removed)
        latitude (float): [rad] Latitude north of the Equator
        longitude (float): [rad] Longitude positive East of Greenwich

    Returns:
        (datetime): Time of transit that day
    """
    sun = Sun()
    obs = Observer()
    obs.lat = latitude
    obs.lon = longitude
    obs.date = day.date()

    return obs.next_transit(sun).datetime()


def sunset(day, latitude, longitude=0.):
    """Time of sunset that day.

    Args:
        day (datetime): day of computation (the HH:MM:SS will be removed)
        latitude (float): [rad] Latitude north of the Equator
        longitude (float): [rad] Longitude positive East of Greenwich

    Returns:
        (datetime): Time of sunset that day
    """
    sun = Sun()
    obs = Observer()
    obs.lat = latitude
    obs.lon = longitude
    obs.date = day.date()

    return obs.next_setting(sun).datetime()


def day_length(day, latitude, longitude=0.):
    """Duration of day light.

    Args:
        day (datetime): day of computation (the HH:MM:SS will be removed)
        latitude (float): [rad] Latitude north of the Equator
        longitude (float): [rad] Longitude positive East of Greenwich

    Returns:
        (float): [h] duration of daylight that day
    """
    return (sunset(day, latitude, longitude) - sunrise(day, latitude, longitude)).total_seconds() / 3600


def elevation(date, latitude, longitude=0.):
    """Compute solar elevation (aka altitude).

    Args:
        date (datetime): date and time of computation (solar time if longitude=0)
        latitude (float): [rad] Latitude north of the Equator
        longitude (float): [rad] Longitude positive East of Greenwich

    Returns:
        (float): solar elevation [rad]
    """
    sun = Sun()
    obs = Observer()
    obs.lat = latitude
    obs.lon = longitude
    obs.date = date
    sun.compute(obs)

    return sun.alt


def declination(date):
    """Declination angle of the sun.

    Args:
        date (datetime): day of computation (the HH:MM:SS will be removed)

    Returns:
        (float): [rad] declination of sun
    """
    sun = Sun()
    sun.compute(date)

    return sun.dec


def solar_hour(date, latitude, longitude=0.):
    """Position of sun around the earth.

    solar_hour_angle expressed in hours instead of angle.

    Args:
        date (datetime): date and time of computation (solar time if longitude=0 else UTC)
        latitude (float): [rad] Latitude north of the Equator
        longitude (float): [rad] Longitude positive East of Greenwich

    Returns:
        (float): [h] solar hour to solar noon
    """
    return (date - solar_noon(date, latitude, longitude)).total_seconds() / 3600


def solar_hour_angle(date, latitude, longitude=0.):
    """Position of sun around the earth.

    Observing the sun from earth, the solar hour angle is an expression of time,
    expressed in angular measurement, usually degrees, from solar noon. At solar
    noon the hour angle is zero degrees, with the time before solar noon expressed
    as negative degrees, and the local time after solar noon expressed as positive
    degrees.

    For example, at 10:30 AM local apparent time the hour angle is −22.5°
    (15° per hour times 1.5 hours before noon).

    Args:
        date (datetime): date and time of computation (solar time if longitude=0 else UTC)
        latitude (float): [rad] Latitude north of the Equator
        longitude (float): [rad] Longitude positive East of Greenwich

    Returns:
        (float): [rad] solar hour angle
    """
    return solar_hour(date, latitude, longitude) * pi / 12
