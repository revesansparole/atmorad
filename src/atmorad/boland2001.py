"""
Implementation of formalisms from Boland 2001.
"""
from math import exp


def diffuse_fraction(kt):
    """Fraction of diffuse light

    References: Boland (2001) eq2

    Args:
        kt (float): [-] clearness index, ratio rg measured on extraterrestrial radiation

    Returns:
        (float): [-] diffuse fraction
    """

    return 1 / (1 + exp(7.997 * (kt - 0.586)))
