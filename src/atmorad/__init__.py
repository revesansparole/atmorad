"""
Set of formalisms to deal with radiation in the atmosphere for agronomy and ecology mainly
"""
# {# pkglts, src
# FYEO
# #}
# {# pkglts, version, after src
from . import version

__version__ = version.__version__
# #}
