"""
Discretize daily to hourly radiation
====================================

Plot different formalisms to extrapolate daily radiation to hourly time step.
"""
from datetime import datetime, timedelta
from importlib import import_module
from math import radians

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from atmorad.astrosun import day_length, solar_hour, solar_hour_angle, sunrise, sunset

formalisms = ('baig1991',
              # 'collares1979',
              'garg1987',
              # 'gueymard1986',
              # 'jain1984',
              # 'jain1988',
              # 'liu1960',
              # 'newell1983',
              # 'shazly1996',
              # 'whillier1956',
              )

mods = {form: import_module(f"atmorad.{form}") for form in formalisms}

day = datetime(2020, 6, 1)
rg_day = 30  # [MJ.m-2] overall radiation received that day
lat = radians(43)
long = radians(4)

records = []
for dh in np.linspace(0, 24, 100):
    date = day + timedelta(hours=dh)
    record = dict(
        date=date,
        dh=dh,
    )

    for form, mod in mods.items():
        try:
            record[form] = mod.day_course(rg=rg_day,
                                          tsn=solar_hour(date, lat, long),
                                          day_length=day_length(date, lat, long))
        except TypeError:
            record[form] = mod.day_course(rg=rg_day,
                                          w=solar_hour_angle(date, lat, long),
                                          ws=solar_hour_angle(sunset(date, lat, long), lat, long))

    records.append(record)

df = pd.DataFrame(records).set_index(['date'])
df['dh'] = df['dh'] - df['dh'].shift()

fig, axes = plt.subplots(1, 1, figsize=(11, 5), squeeze=False)
ax = axes[0, 0]
ax.set_title(f"{day.date().isoformat()}")

for formalism in formalisms:
    ax.plot(df.index, df[formalism], label=f"({(df[formalism] * df['dh']).sum() * 3600e-6:.2f}) {formalism}")

ax.legend(loc='upper left', title=f"daily:{rg_day:.2f} [MJ.m-2]")

ax.axhline(y=0, ls='--', color='#0000aa')
ax.axvline(sunrise(day, lat, long), ls='--', color='#aaaaaa')
ax.axvline(sunset(day, lat, long), ls='--', color='#888888')

ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
ax.set_ylabel("radiation [W.m-2]")

fig.tight_layout()
plt.show()
