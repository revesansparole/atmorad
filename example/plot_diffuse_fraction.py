"""
Diffuse fraction estimation
===========================

Plot simulated evolution of diffuse fraction throughout the day.
"""
from datetime import datetime, timedelta
from math import radians

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd
from atmorad.astrosun import day_length, solar_hour, solar_noon, sunrise, sunset
from atmorad.baig1991 import hourly
from atmorad.boland2001 import diffuse_fraction
from atmorad.maleki2017 import daily_extraterrestrial_radiation, hourly_extraterrestrial_radiation

day = datetime(2019, 6, 1)
latitude = radians(43.742389)
longitude = radians(5.1268)
rg_day = daily_extraterrestrial_radiation(day, latitude) * 0.8
dl = day_length(day, latitude, longitude)

# estimate hourly radiation
records = []
for h in range(24):
    date = day + timedelta(hours=h)
    records.append(dict(
        date=date,
        rg=hourly(rg_day, solar_hour(date, latitude, longitude), dl),
        etr=hourly_extraterrestrial_radiation(date-timedelta(hours=0.5), latitude, longitude),
    ))

df = pd.DataFrame(records).set_index('date')

# estimate diffuse fraction
df['kt'] = df['rg'] / df['etr']

df['diff_frac'] = df['kt'].apply(lambda kt: diffuse_fraction(kt)).fillna(1.)

# plot result
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(11, 8), squeeze=False, gridspec_kw={'height_ratios': [2, 1, 1]})
ax = axes[0, 0]

ax.plot(df.index, df['diff_frac'], label="Boland (2001)")
ax.legend(loc='upper left')
ax.set_ylabel("diff frac [-]")

ax = axes[1, 0]
ax.plot(df.index, df['rg'], label="measured rg")
ax.plot(df.index, df['etr'].clip(lower=0), '--', label="extra terrestrial rg")
ax.axvline(sunrise(day, latitude, longitude), ls='--', color='#aaaaaa')
ax.axvline(solar_noon(day, latitude, longitude), ls='--', color='#999999')
ax.axvline(sunset(day, latitude, longitude), ls='--', color='#888888')

ax.legend(loc='upper left')
ax.set_ylabel("[MJ.m-2.h-1]")

ax = axes[2, 0]
ax.plot(df.index, df['kt'])
ax.set_ylabel("clearness index [-]")

ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))

fig.tight_layout()
plt.show()
